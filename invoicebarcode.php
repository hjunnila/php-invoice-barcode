<?php
/****************************************************************************************
 *
 * Create finnish invoice bar code in CODE 128 format using Picqer PHP Barcode Generator
 * from https://github.com/picqer/php-barcode-generator, which is licensed with GNU LGPLv2.
 *
 * This file is licensed under the MIT license, 
 * copyright (c) Heikki Junnila
 *
 ****************************************************************************************/

require 'vendor/autoload.php';

/**
 * Output the given invoice data as a bar code.
 *
 * @param iban Recipient's account number in IBAN format without the preceding country code, e.g. Actual IBAN "FI11073500123685" is represented as "1107350000123685"
 * @param euros Euros part of the amount (e.g. 607 if the total amount is 607,20)
 * @param cents Cents part of the amount (e.g. 20 if the total amount is 607,20)
 * @param reference Reference number
 * @param date A DateTime object containing day, month and year when the payment is due
 */
function invoiceBarcode($iban, $euros, $cents, $reference, $date) {
    $len = strlen($iban);
    if ($len != 16) {
        echo 'IBAN length ('.$len.') error.';
        return;
    }

    $euros = str_pad($euros, 6, '0', STR_PAD_LEFT);
    $cents = str_pad($cents, 2, '0', STR_PAD_LEFT);
    $reference = str_pad($reference, 20, '0', STR_PAD_LEFT);

    $data = "4".$iban.$euros.$cents.'000'.$reference.$date->format("ymd");
    
    $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
    echo $generator->getBarcode($data, $generator::TYPE_CODE_128, 2, 80);
    echo "Virtual barcode data: ".$data;
}

invoiceBarcode('7944052020036082', 4883, 15, '868516259619897', new DateTime('2010-06-12'));

?>
