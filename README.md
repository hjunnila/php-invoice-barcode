# php-invoice-barcode

Generates Finnish invoice bar codes (version 4) as specified by FK in "Pankkiviivakoodi-opas".

# Installation

Run 'composer install' to get the dependencies installed under vendor/. After successful execution,
opening invoicebarcode.php in your browser thru an otherwise correctly configured PHP-capable web server
(try MAMP if you can't come up with anything else) displays a demo invoice barcode.
